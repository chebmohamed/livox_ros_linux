#! /bin/bash
source /opt/ros/melodic/setup.sh 
echo "Ros environement sourcing complete"
cd ~/Livox/ws_livox
catkin_make
echo "Building complete"
source ./devel/setup.sh
echo "Devel sourcing complete"
echo "Connecting to sensor ...."
roslaunch livox_ros_driver livox_lidar_rviz.launch
